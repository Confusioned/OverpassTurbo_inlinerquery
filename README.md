# Radinfrastrukturabfrage für Magdeburg in OpenStreetMap

## Anleitung

-Code auf die Seite https://overpass-turbo.eu/ kopieren

-unter "Einstellungen" -> "Karte"  -> die Option ```Kleine Features nicht wie POIs darstellen``` aktivieren

-auf Ausführen klicken

-auf Magdeburg zoomen

-für OSM-Details auf das entsprechende Wegsegment klicken

## Legende

|Linie        |Wegart|
|-------------|------|
| keine       |Radinfrastruktur - noch nicht erfasst|
|blau schmal  |„Radinfrastruktur“, baulich als reiner Radweg ausgeführt, (Fuß-)Wege mit separater Radspur oder unvollständig erfasster Weg mit Zeichen 237/240/241|
|blau breit   |benutzungspflichtige Radinfrastruktur mit Zeichen 237/240/241|
|schwarz breit|Fahrradstraße Zeichen 244|
|rot breit    |Fahrradspur auf der Fahrbahn|
|grün schmal  |Weg mit Beschilderung "Radfahrer frei"
|grau schmal  |unbeschilderter Weg - Radfahren zulässig, unklassifierter Weg wie z.B. Verbindungsweg an Grundstücken, Parkweg, etc. oder unvollständig erfasster Radweg |

## Code für OverpassTurbo

```
//Magdeburger Radinfrastruktur farbig nach Wegart
  
[out:json][timeout:25];
area(3600062481)->.searchArea;
  
( // Suchkriterien Wegeigenschaften
    way[~"bicycle"~"yes|designated"](area.searchArea); //Radweg
    way["cycleway"~"yes|designated"](area.searchArea);
    way["highway"="cycleway"](area.searchArea);
    way["bicycle_road"~"yes|designated"](area.searchArea);       //Fahrradstraße
    way["bicycle"="lane"](area.searchArea);           //Fahrradspur
    way[~"cycleway"~"lane"](area.searchArea);
    way["bicycle:lanes"~"yes"](area.searchArea);
    way[~"traffic_sign"~"1022"](area.searchArea);     //Fahrrad frei
    way["highway"="path"]["bicycle"="yes"](area.searchArea);
    way["highway"="footway"]["bicycle"="yes"](area.searchArea);
  );  
  out body; // print results  
  >;  
  out skel qt;

//Linienartvorgaben für Visualisierung
{{style:
  way
  { color:grey; opacity:1;width:2; }
  
  way[bicycle=yes]
  { color:grey; opacity:1; width:2; }
  
  way[segregated=yes],
  way[highway=cycleway]
  { color:blue; opacity:1; width:2; }
  
  way[traffic_sign=DE:1022-10],
  way[traffic_sign=DE:1022-10,1000-33],
  way[traffic_sign=DE:239,1022-10],
  way[traffic_sign:forward=DE:239,1022-10],
  way[traffic_sign:forward=DE:239,1022-10,1000-31],
  way[traffic_sign=DE:239,1000-33],
  
  way[traffic_sign=DE:239,DE:1022-10],
  way[traffic_sign=DE:239;DE:1022-10],
  way[traffic_sign=DE:239;1022-10],
  way[traffic_sign=DE:239;1000-33],
  way[traffic_sign=DE:239,DE:1000-33],
  
  way[traffic_sign=DE:242,1022-10]
  { color:green; opacity:1; width:2; }
  
  way[traffic_sign=DE:237],
  way[traffic_sign=DE:237,1000-31],
  way[traffic_sign:forward=DE:237],
  way[traffic_sign:backward=DE:237]
   { color:blue; opacity:1; width:4; }
  
  way[traffic_sign=DE:240],
  way[traffic_sign:forward=DE:240],
  way[traffic_sign=DE:240,1000-31],
  way[traffic_sign:forward=DE:240,1000-31],
  way[traffic_sign:backward=DE:240],
  way[traffic_sign:backward=DE:240,1000-31],
  way[traffic_sign=DE:240,1026-35]
  { color:blue; opacity:1; width:4; }
  
  way[traffic_sign=DE:241],
  way[traffic_sign=DE:241,1000-31],
  way[traffic_sign=DE:241-30],
  way[traffic_sign=DE:241-30,1000-31],
  way[traffic_sign:forward=DE:241],
  way[traffic_sign:forward=DE:241-30],
  way[traffic_sign:forward=DE:241,1000-31],
  way[traffic_sign:forward=DE:241-30,1000-31],
  way[traffic_sign:forward=DE:265;241-30]
  { color:blue; opacity:1; width:4; }
  
  way[bicycle_road=yes],
  way[bicycle_road=designated]
  { color:black; opacity:1; width:4; }
  
  way[cycleway=lane],
  way[cycleway:left=lane],
  way[cycleway:right=lane],
  way[cycleway:both=lane],
  way[cycleway:both=shared_lane],
  way[cycleway=opposite_lane],
  way[bicycle:lanes],
  way[bicycle:lanes:forward],
  way[bicycle:lanes:backward]
  { color:red; opacity:1; width:4; }
  
  way[access=private]
  { color:grey; opacity:0; width:2; }
}}
```
## OpenStreetMap-Dokumentation

-Beschreibung der Werte und Schlüssel: https://wiki.openstreetmap.org/wiki/DE:Map_Features